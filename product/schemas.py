from pydantic import BaseModel, EmailStr
from typing import List, Optional

class Product(BaseModel):
    name  : str
    description : str
    seller_email: str
    price : int
    
   
class DisplayProduct(BaseModel):
    name  : str
    description : str
    price : int
    

    class Config:
        orm_mode = True

class Seller(BaseModel):
    username  : str
    email     : str
    password  : str
    phone     : str
    address   : str
    address2  : Optional[str] =  None
    #address3  : Optional[str] =  None


class SellerDisplay(BaseModel):
    username : str
    email : str
    phone : str
    address   : str
    #address2  : str
    

    class Config:
        orm_mode = True

class Login(BaseModel):
    username : str
    password : str


class Token(BaseModel):
    access_token : str
    token_type   : str

class TokenData(BaseModel):
    username : Optional[str] = None

class MyProduct(BaseModel):
    username : str
    products : List[Product]
    

    class Config:
        orm_mode = True

class Media(BaseModel):
    username : str

class EmailSchema(BaseModel):
    email: List[EmailStr]
    body : str