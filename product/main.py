from fastapi import FastAPI
from .database import  engine
from .import models
from .routers import product, seller, login
#from fastapi_mail import FastMail, MessageSchema,ConnectionConfig
import uvicorn



app = FastAPI(
    title = "Product API",
    description = "Get details about Products and Sellers",
    terms_of_service = "http://www.google.com",
    contact = {
        "Name" : "Shubham Verma",
        "Email": "vjohn872@gmail.com",
        "Phone": "80523333"
    },
    license_info = {
        "name": "ISO:2020",
        "url" : "http://www.google.com"
    },
    docs_url = "/",
    redoc_url= None 
)
#models.Base.metadata.create_all(engine)
models.Base.metadata.create_all(engine)

app.include_router(product.router)
app.include_router(seller.router)
app.include_router(login.router)


if __name__ == '__main__' :
    uvicorn.run(app, host="0.0.0.0", port=8001)