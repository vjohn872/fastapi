from re import S
from sqlalchemy.sql.operators import startswith_op
from sqlalchemy.sql.selectable import FromClause
from .database import Base
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils import  EmailType, URLType

class Product(Base):

    __tablename__ = "products"

    id = Column(Integer, primary_key = True)
    name = Column(String)
    description = Column(String)
    price = Column(Integer)
    seller_email = Column(String, ForeignKey('sellers.email'))
    seller = relationship('Seller', back_populates='products')

class Seller(Base):

    __tablename__ = "sellers"

    email    = Column(String, primary_key = True, index = True)
    username = Column(String)
    password = Column(String)
    phone    = Column(String)
    address  = Column(String)
    address2 = Column(String, default = None)
    #address3 = Column(String, default = None)

    products = relationship("Product", back_populates='seller')

class Media(Base):

    __tablename__ = "media"

    id       = Column(Integer, primary_key = True)
    username = Column(String)
    url      = Column(URLType)

