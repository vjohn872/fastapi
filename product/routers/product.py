from fastapi import APIRouter, Depends, status, HTTPException, File, UploadFile
import shutil
from sqlalchemy.orm import Session
from ..import schemas, models
from ..database import Base, get_db
from ..models import Product, Media
from typing import List
from product.routers.login import get_current_user

router = APIRouter(
    tags = ['Product'],
    prefix = "/product"
)

@router.post('/', status_code = status.HTTP_201_CREATED)
def create_product(request : schemas.Product , db:Session = Depends(get_db)):
    new_product = Product(name = request.name, description = request.description, 
                            price = request.price, seller_email = request.seller_email)
    db.add(new_product)
    db.commit()
    db.refresh(new_product)
    return new_product

@router.get('/',response_model=List[schemas.DisplayProduct])
def all_products(db:Session = Depends(get_db)):  #current_user : schemas.Seller = Depends(get_current_user)
    return db.query(Product).all()

@router.get('/{id}',response_model=schemas.DisplayProduct, status_code = status.HTTP_302_FOUND)
def specific_product(id, db:Session = Depends(get_db)):
    product = db.query(Product).filter(Product.id == id).first()
    if not product:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = "Product not found")
    return product

@router.put('/{id}')
def update_product(request : schemas.Product, id, db:Session = Depends(get_db)):
    product = db.query(Product).filter(Product.id == id)
    if not product.first():
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = "Product not found")
    else: 
        product.update(request.dict())
        db.commit()
        return {"Successfully updated product"}

@router.delete('/{id}')
def delete_product(id, db:Session = Depends(get_db)):
    product = db.query(Product).filter(Product.id == id)
    if not product.first():
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = "Product not found")
    else: 
        product.delete(synchronize_session = False)
        db.commit()
        return {f"{id} product is deleted permanently"}

img = ['jpg', 'jpeg', 'png']
text = ['txt', 'odt', 'pdf']
@router.post('/files', status_code = status.HTTP_201_CREATED)
def uplaod_file(db: Session = Depends(get_db),current_user : schemas.Seller = Depends(get_current_user), file : UploadFile = File(...) ):
    user = db.query(models.Seller).filter(models.Seller.username == current_user).first()

    f = file.filename.split('.')
    if f[1] in img:
        fil_loc = 'product/Media/Images/' + file.filename
        with open(fil_loc, 'wb') as file1:
            shutil.copyfileobj(file.file, file1)

    elif f[1] in text:
        fil_loc = 'product/Media/Text/' + file.filename
        with open(fil_loc, 'wb') as file1:
            shutil.copyfileobj(file.file, file1)
    
    url = str(fil_loc)
    new_file = Media(username = user.username , url = url)
    db.add(new_file)
    db.commit()
    db.refresh(new_file)
    return new_file
    