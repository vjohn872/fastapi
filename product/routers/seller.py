from fastapi import APIRouter
from ..import schemas, models
from fastapi.params import Depends
from sqlalchemy.orm import Session
from ..database import get_db
from typing import List
from fastapi import status, HTTPException
from passlib.context import CryptContext
from product.routers.login import get_current_user
from fastapi_mail import MessageSchema, FastMail, ConnectionConfig
from starlette.responses import JSONResponse
#from dotenv import dotenv_values


#credentials = dotenv_values(".env")

conf = ConnectionConfig(
    MAIL_USERNAME = "vjohn872@gmail.com",
    MAIL_PASSWORD = "Shubham@123",
    MAIL_FROM = "vjohn872@gmail.com",
    MAIL_PORT = 587,
    MAIL_SERVER = "smtp.gmail.com",
    MAIL_TLS = True,
    MAIL_SSL = False,
    USE_CREDENTIALS = True
)



router = APIRouter(
    tags=['Seller'],
    prefix="/seller"
)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

#response_model=List[schemas.SellerDisplay]
#@app.get('/sellers')
@router.get('/')
def all_sellers(db: Session = Depends(get_db)):
    sellers = db.query(models.Seller).all()
    return sellers 

@router.post('/', response_model=schemas.SellerDisplay)
def add_seller(request: schemas.Seller, db: Session = Depends(get_db)):
    hashedpass = pwd_context.hash(request.password)
    new_seller = models.Seller(
        username = request.username, email = request.email, 
        password = hashedpass, phone = request.phone, address = request.address,
        address2 = request.address2 
        )
    db.add(new_seller)
    db.commit() 
    db.refresh(new_seller)    

    return new_seller

@router.get('/MyProduct')
def my_product(db: Session = Depends(get_db),current_user : schemas.Seller = Depends(get_current_user)):
    user = db.query(models.Seller).filter(models.Seller.username == current_user).first()
    product = db.query(models.Product).filter(models.Product.seller_email == user.email).all()
    return {"user" : user.email, "Products" : product} 

@router.post("/email")
async def simple_send(email1: schemas.EmailSchema, current_user : schemas.Seller = Depends(get_current_user)):

    message = MessageSchema(
        subject="Fastapi-Mail module",
        recipients=email1.email,  # List of recipients, as many as you can pass 
        body=email1.body
        )

    fm = FastMail(conf)
    await fm.send_message(message)
    return JSONResponse(status_code=200, content={"message": "email has been sent"})     
